from django.db import models

# Create your models here.

class Educator (models.Model):
        id =  models.CharField(max_length=32, primary_key=True)
        firstName = models.CharField(max_length=64)
        lastName =  models.CharField(max_length=64)
        email = models.EmailField()
        update_count = models.IntegerField(default=0)
    

        
        def __str__(self):
            return self.firstName and self.lastName and self.id and self.email
        
    
class Position(models.Model): 
    educator = models.ForeignKey(Educator, on_delete=models.CASCADE)
    worksite = models.TextField(max_length=48)
    title = models.TextField(max_length=48)
    category = models.TextField(max_length=48)
    update_count = models.IntegerField(default=0)

    def Position (self):
        return self.educator.id and self.educator.firstName and self.Educator.lastName and self.Educator.email