# Generated by Django 4.0.4 on 2022-05-27 18:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('educators', '0002_alter_educator_aps_id_alter_educator_category_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Educator',
            new_name='Educators',
        ),
    ]
