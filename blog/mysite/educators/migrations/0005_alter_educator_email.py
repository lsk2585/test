# Generated by Django 4.0.4 on 2022-06-01 15:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('educators', '0004_educator_position_delete_educators'),
    ]

    operations = [
        migrations.AlterField(
            model_name='educator',
            name='email',
            field=models.CharField(max_length=64),
        ),
    ]
