import django_filters

from .models import Educator, Position
from django.db.models import Q
from django.forms import * 

'''
class PositionFilter(django_filters.FilterSet):
    class Meta: 
        model = Position
        fields = {'worksite': ['icontains'], 
            'category': ['icontains'], 
            'title': ['icontains']}
'''
class EducatorFilter(django_filters.FilterSet):
    #name = django_filters.CharFilter(lookup_expr='iexact')
    #ordering = django_filters.ChoiceFilter(label='Ordering', choices=CHOICES, method='filter_by_ordering')
    #positions = PositionFilter()
    position__worksite__icontains = django_filters.CharFilter('position__worksite')
    position__category__icontains = django_filters.CharFilter('position__category')
    position__title__icontains = django_filters.CharFilter('position__title')


    class Meta:
        model = Educator
        fields = {'firstName': ['icontains'], 
            'lastName': ['icontains'], 
            'email': ['icontains'], 
            'position__worksite': ['icontains'], 
            'position__category': ['icontains'], 
            'position__title': ['icontains'],}