from django.core.management.base import BaseCommand
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from datetime import date
import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import json

import markdown

import time
import os

from educators.models import Position, Educator


class Command(BaseCommand):
    help = 'import booms'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        
        with open('aps_staff.md', 'r') as f:
            text = f.read()
            html = markdown.markdown(text)

        with open('aps_staff_lst.html', 'w') as f:
            f.write(html)

        # Read the list with all the links
        aps_list_path = os.getcwd() + "/aps_staff_lst.html"

        aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")

        # All the links to aps websites
        all_links_aps = []

        for a in aps_list_html_page.find_all('a', href=True):
            all_links_aps.append(a['href'])

        def arrayToString(array):
            items = str(array)
            items = items.replace("[", "")
            items = items.replace("]", "")
            items = items.replace("\'", "")
            return items

        all_educators = []
        today = int(date.today().strftime("%Y%m%d"))
        for link in all_links_aps:
                html = urllib.request.urlopen(link).read()
                soup = BeautifulSoup(html, 'html.parser')
                scripts = soup.find_all('script')
                
                found_data = 0

                for script in scripts:
                    result = script.text.find('aps_directory_data')
                    if (result != -1):
                        raw_data = script.text.strip()[21:-1]
                        
                data = json.loads(raw_data)

                for key in data:
                    worksite = arrayToString(data[key]['site'])
                    worksite = worksite.replace("5001", "Virtual Elementary School")
                    worksite = worksite.replace("5002", "Virtual Middle School")
                    worksite = worksite.replace("5003", "Virtual High School")

                    try:
                        title = arrayToString(data[key]['title'])
                    except:
                        title = 'n/a'

                    educator = Educator.objects.update_or_create(id=data[key]['apsid'], defaults={"firstName": data[key]['name_f'], "lastName": data[key]['name_l'], "email": data[key]['email'], "update_count": today})[0]
                    educator.save()

                    position = Position.objects.update_or_create(educator=educator, worksite=worksite, title=title, category=arrayToString(data[key]['category']), defaults={"update_count": today})[0]
                    position.save()

        for educator in Educator.objects.all():
            if educator.update_count < today:
                educator.delete()
        
        for position in Position.objects.all():
            if position.update_count < today:
                position.delete()
'''
    app = Celery()

    @app.task
    def test(arg):
        print(arg)

    @app.on_after_configure.connect
    def setup_periodic_tasks(sender, **kwargs):
        # Calls test('hello') every 10 seconds.
        #sender.add_periodic_task(10.0, test.s('hello'), name='add every 10')

        # Calls test('world') every 30 seconds
        #sender.add_periodic_task(30.0, test.s('world'), expires=10)

        # Executes every Thursday at 12:05pm on the second day of every month
        sender.add_periodic_task(
            crontab(hour=12, minute=5, day_of_week=4, day_of_month=2),
            test.s('Happy Mondays!'),
        )

    @app.task
    def add(x, y):
        z = x + y
        print(z)

'''