from django.urls import path, include
from . import views
from .models import Educator, Position



urlpatterns = [
    #path('educators/<int:educators_id>', views.educators_by_id, name='educators_by_id'),
    path('all_educators', views.all_educators),
    path('filters', views.educator_filter_list),
    
    ]