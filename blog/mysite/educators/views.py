from django.shortcuts import render
from urllib import request
from .models import Educator, Position
from .filters import EducatorFilter
import django_filters
from django.http import HttpResponse
from django.views.generic import ListView, DetailView


# Create your views here.
'''
def educators_by_id (request, educators_id):
    educators = Educators.objects.get(pk=educators_id)
    return render(request, "educators/educators.html", {'Educators': educators})
'''


def all_educators(request):
    positions = Position.objects.select_related('educator').all().order_by('id')
    
    return render(request, "educators/all_educators.html", {'Positions': positions})

def educator_filter_list(request):
    
    #position_filter = PositionFilter(request.GET, queryset=Position.objects.all())
    filter = EducatorFilter(request.GET, queryset=Educator.objects.all())
    #for educator in Educator.objects.all():
      #positions_list.append(Position.objects.filter(id=educator.id[1:]))
    #positions = EducatorFilter(request.GET, queryset=Position.objects.all())
    #positions = EducatorFilter(request.GET, queryset=)
    return render(request, 'educators/filter.html', {'filter': filter})


'''
educator_positions = []
Educators = Educator.objects.all()
for each in Educators:
    positions = each.position_set.all()
    educator_positions.append(positions)
'''